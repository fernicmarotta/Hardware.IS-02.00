# Hardware.IS-02.00

## Interfaz de Semáforo

Autor: 

- Agustín Gonzalez

Revisión: 

- Germán Vazquez

Hardware:

- 2 displays 7 segmentos.
- 5 LEDs de Potencia.
- Buffer 74HCT541 + conversor CD4511
- Buffer 74HCT541 + 5 MOSFET IRLML6244PBF

Software:

- Eagle 6.5.0 Light

Usos:

- Semáforo